# Makefile

OBJS= controller.o dataxfer.o telnet.o devcfg.o readconfig.o selector.o ser2net.o utils.o usbFindSerial.o

all:: ser2net
all:: ser2net.man

clean::
	rm -f *.o
	rm -f ser2net

ser2net: $(OBJS)
	$(CC) -o $@ $^

%.man: %.8
	nroff -man $< > $@

%.o: %.c
	$(CC) -c $< -O2 -g -Wall -DVERSION=\"ser2net-KO1\" -I.

controller.o: controller.c controller.h selector.h dataxfer.h
dataxfer.o:   dataxfer.c dataxfer.h controller.h selector.h devcfg.h utils.h
devcfg.o:     devcfg.c devcfg.h controller.h
readconfig.o: readconfig.c dataxfer.h controller.h readconfig.h
selector.o:   selector.c selector.h
ser2net.o:    ser2net.c readconfig.h controller.h utils.h selector.h dataxfer.h
utils.o:      utils.c utils.h

#end file
